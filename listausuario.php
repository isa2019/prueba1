<?php
include 'sesion.php';
include "lib/config.php";
include "lib/Database.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Listado de Usuarios</title>
  <meta name="KEYWORDS" content="Sesion en linea template"> 
  <meta name="descripcion" content="pagina mejorada con php">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="shortcut icon" type="image/x-icon" href="img/ico.jpg">
	<link rel="stylesheet" type="text/css" href="css/fondito.css">


</head>
<body>

	<section class="container">
    <div class="row">
      <div class="col-sm-12">
        <?php
        $db= new Database();
        $query="SELECT * FROM tbl_login";
        $read = $db->select($query);
        ?>
        <?php
        if(isset($_GET['msg'])){
        echo "<div class='alert alert-primary'><span>".$_GET['msg']."</span></div>"; 
      } 
      ?>
      </div>
      <div class="col-sm-12">
        <table class="table table-hover">
          <thead class="thead-dark">
            <tr>
              <th scope="col">id_unico</th>
              <th scope="col">Nombre de Usuario</th>
              <th scope="col">Password</th>
              <th scope="col">Fecha de Ingreso al sistema</th>
              <th scope="col">Foto</th>
              <th scope="col">Editar</th>
              <th scope="col">Eliminar</th>

            </tr>
          </thead>
          <?php if($read){?>
          <?php
          $i=1;
          while($row=$read->fetch_assoc()){
          ?>
          <tbody>
            <tr>
              <td><?php echo $row['id_login'];?></td>
              <td><?php echo $row['user'];?></td>
              <td><?php echo $row['password'];?></td>
              <td><?php echo $row['f_inicio'];?></td>
              <td><img class="img-thumbnail" width="100px" src="img/<?php echo $row['foto'];?>"></td>
              <td><a href="updateusuario.php?id_login=<?php echo urlencode($row['id_login']);?>" class="btn btn-primary btn-sm">Editar</a></td>
               <td><a href="deleteusuario.php?delete=1&id_login=<?php echo urlencode($row['id_login']);?>" class="btn btn-primary btn-sm">Eliminar</a></td>
            </tr>
          </tbody>
            <?php } ?>
            <?php } else { ?>
            <p> Los datos no son validos!!</p>
            <?php } ?>
        </table>
        <div class="form-group">
          <label>
            <span><a href="principal.php" class="btn btn-info">Ir a principal</a></span>
            <span><a href="logout.php" class="btn btn-warning">Salir de sistema</a></span>
            <span><a href="personal.php" class="btn btn-success">Registrar Personal</a></span>
             <span><a href="registro.php" class="btn btn-secondary">Registrar Nuevo Usuario</a></span>
               <span><a href="index_excel.php" class="btn btn-primary">Exportar a Excel</a></span>
               <span><a href="index_word.php" class="btn btn-danger">Exportar a Word</a></span>
                <span><a href="index_pdf.php" class="btn btn-secondary">Exportar a Pdf</a></span>
          </label>
          
      </div>
  
    </div>
</div>
	</section>	
 <script src="js/jquery-3.3.1.min.js"></script>
		  <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>


</body>
</html>