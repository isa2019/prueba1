<?php
include 'sesion.php';
include "lib/config.php";
include "lib/Database.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Mostrar Articulo</title>
  <meta name="KEYWORDS" content="Sesion en linea template"> 
  <meta name="descripcion" content="pagina mejorada con php">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="shortcut icon" type="image/x-icon" href="img/ico.jpg">
	<link rel="stylesheet" type="text/css" href="css/fondito1.css">


</head>
<body>

	<section class="container">
    <div class="row">
      <div class="col-sm-12">
        <?php
        $db= new Database();
        $query="SELECT * FROM tbl_articulo";
        $read = $db->select($query);
       
        ?>
        <?php
        if(isset($_GET['msg'])){
        echo "<div class='alert alert-primary'><span>".$_GET['msg']."</span></div>"; 

      } 
      ?>
          <?php if($read){?>
          <?php
          $i=1;
          while($row=$read->fetch_assoc()){
          ?>
 
      </div>

      
      <div class="col-sm-4">
      
        
     
       <article>
        <div class="card h-100 bg-warning">
       <h2 class="card-header text-primary"> <?php echo $row['titulo'];?></h2>
       <br>
         <img class="mx-auto" width="100px" height="75px" src="img/<?php echo $row['foto'];?>">
         <div class="card-body">
         <p class="card-text text-justify">"<?php echo $row['articulo'];?>"</p>
         <p class="text-center text-danger">"<?php echo $row['fecha'];?>"</p>
         <a href="updatearticulo.php?id_arti=<?php echo urlencode($row['id_arti']); ?>" class="btn btn-primary btn-sm">Editar</a>
          <td><a href="deletearticulo.php?delete=1&id_arti=<?php echo urlencode($row['id_arti']); ?>" class="btn btn-danger btn-sm">Eliminar</a></td>
          <br>
          </div>
       </div>

       </article>
      
 
       <?php } ?>
            <?php } else { ?>
            <p> Los datos no son validos!!</p>
            <?php } ?>
            <br>
            </div>
                  
                    <div class="col-sm-12">
                   <div class="form-group">
          <label>
            <span><a href="principal.php" class="btn btn-secondary">Ir a principal</a></span>
            <span><a href="logout.php" class="btn btn-warning">Salir de sistema</a></span>
            <span><a href="personal.php" class="btn btn-secondary">Registrar Personal</a></span>
            <span><a href="inventario.php" class="btn btn-warning">Registrar Inventario</a></span>
             <span><a href="registro.php" class="btn btn-secondary">Registrar Nuevo Usuario</a></span>
                <span><a href="index_word.php" class="btn btn-warning">Exportar a Word</a></span>
                <span><a href="index_pdf.php" class="btn btn-secondary">Exportar a Pdf</a></span>
          </label>
          
      </div>
    </div>

  
    
</div>
	</section>	
 <script src="js/jquery-3.3.1.min.js"></script>
 <script src="js/bootstrap.min.js"></script>
    <!--<script src="js/bootstrap.bundle.min.js"></script>-->


</body>
</html>