<?php
	include 'sesion.php';//Autor: Lic. Marco Antonio dorado Goméz
	include "lib/config.php";
	include "lib/Database.php";
?>
<?php
	$db=new Database();
	if(isset($_POST['submit'])){
	$titulo=mysqli_real_escape_string($db->link, $_POST['titulo']);
	$foto=(isset($_FILES['foto']['name']))?$_FILES['foto']['name']:"";
	$articulo=mysqli_real_escape_string($db->link, $_POST['articulo']);
	
	
	
	if($titulo == '' || $foto == '' || $articulo == ''){
		$error="Los campos no deben estar vacios!!!";
	}else{
		$fecha= new DateTime();
		$nomArchivo=($foto!="")?$fecha->getTimestamp()."_".$_FILES["foto"]["name"]:"avatar.png";
		$tmpfoto=$_FILES["foto"]["tmp_name"];
		//$pass_cifrado = password_hash($contra, PASSWORD_DEFAULT);//encriptando la contraseña
		$query="INSERT INTO tbl_articulo(titulo,foto,articulo) Values('$titulo','$nomArchivo','$articulo')";
			if($tmpfoto !=""){
			move_uploaded_file($tmpfoto, "img/".$nomArchivo);
		}

		$create = $db->insertarticulo($query);
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale=1.0, minimum-scale=1.0">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="shortcut icon" type="image/x-icon" href="img/ico.jpg">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/estilo.css">
		 <script src="js/validper.js"></script>
		<title>Registro de Articulo</title>
</head>
<body>
	<section class="container">
		<div class="row my-5">
			<form class="login" action="articulo.php" method="POST" enctype="multipart/form-data" name="formularito" onsubmit="return ValidaDato()">
				<?php
				if(isset($error)){
					echo"<center><div class='alert alert-danger'><span>".$error."</span></div></center>";

				}
				?>
				<h2><center>Registro de Articulo</center></h2>
			<input type="text" class="campo" placeholder="Ingresar Titulo" name="titulo" id="titulo">
			<div class="form-group">
					<label class="imagen"> Imagen:(*) </label>
					<input type="file" accept="image/*" name="foto" id="foto" value="" placeholder="" class="form-control">
				</div>

      		
      			<textarea name="articulo" value="articulo" rows="10" cols="45">Articulos:</textarea> 
			
				
				<center>
					<button type="submit" name="submit" id="submit" class="btn btn-primary">Guardar</button>
					<a href="principal.php" class="btn btn-danger">Cancelar</a>
				</center>
			</form>
		</div>
					<div class="col text-center"><!-- para que un boton este centrado -->

	<span><a class="btn btn-secondary btn-default btn-lg" href="principal.php">VOLVER A PRINCIPAL</a></span>
    </div>
		</div>


	</section>
	<script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

</body>
</html>