<?php
	/**
	* 
	*/
	class Database
	{
		public $host=DB_HOST;
		public $user=DB_USER;
		public $pass=DB_PASS;
		public $dbname=DB_NAME;

		public $link;
		public $error;
		public function __construct()

		{
			$this->connectDB();
		}

		private function connectDB(){/*instanciando la conexion*/
			$this->link=new mysqli($this->host, $this->user, $this->pass, $this->dbname);
			if(!$this->link){
				$this->error="conexion fallida".$this->link->connect_error;
				return false;


			}

		}
		//seleccionar o leer la base de datos
		public function select($query){
			$result=$this->link->query($query) or die($this->link->error.__LINE__);
			if($result->num_rows > 0){
				return $result;
			}
			else
			{
				return false;
			}
		}
		//insertar datos
	public function insert($query){
		$insert_row=$this->link->query($query)or die($this->link->error._LINE_);
		if($insert_row){
			header("Location:listausuario.php?msg=".urlencode('Datos almacenados exitosamente!!!!'));
			exit();
		}else{
			die("Error:(".$this->link->errno.")".$this->link-error);
		}
	}
		public function insertpersonal($query){
		$insert_row=$this->link->query($query)or die($this->link->error._LINE_);
		if($insert_row){
			header("Location:listapersonal.php?msg=".urlencode('Datos almacenados exitosamente!!!!'));
			exit();
		}else{
			die("Error:(".$this->link->errno.")".$this->link-error);
		}
	}
		public function insertinventario($query){
		$insert_row=$this->link->query($query)or die($this->link->error._LINE_);
		if($insert_row){
			header("Location:listainventario.php?msg=".urlencode('Datos almacenados exitosamente!!!!'));
			exit();
		}else{
			die("Error:(".$this->link->errno.")".$this->link-error);
		}
	}
		public function insertarticulo($query){
		$insert_row=$this->link->query($query)or die($this->link->error._LINE_);
		if($insert_row){
			header("Location:MostrarArticulo.php?msg=".urlencode('Datos almacenados exitosamente!!!!'));
			exit();
		}else{
			die("Error:(".$this->link->errno.")".$this->link-error);
		}
	}
	//actualizar datos
	public function update($query){
			$update_row=$this->link->query($query)or die($this->link->error._LINE_);
		if($update_row){
			header("Location:listausuario.php?msg=".urlencode('Los datos han actualizados exitosamente!!!!'));
			exit();
		}else{
			die("Error:(".$this->link->errno.")".$this->link-error);
		}
	}
	public function updatepersonal($query){
			$update_row=$this->link->query($query)or die($this->link->error._LINE_);
		if($update_row){
			header("Location:listapersonal.php?msg=".urlencode('Los datos han actualizados exitosamente!!!!'));
			exit();
		}else{
			die("Error:(".$this->link->errno.")".$this->link-error);
		}
	}
	public function updateinventario($query){
			$update_row=$this->link->query($query)or die($this->link->error._LINE_);
		if($update_row){
			header("Location:listainventario.php?msg=".urlencode('Los datos han actualizados exitosamente!!!!'));
			exit();
		}else{
			die("Error:(".$this->link->errno.")".$this->link-error);
		}
	}
		public function updatearticulo($query){
			$update_row=$this->link->query($query)or die($this->link->error._LINE_);
		if($update_row){
			header("Location:MostrarArticulo.php?msg=".urlencode('Los datos han actualizados exitosamente!!!!'));
			exit();
		}else{
			die("Error:(".$this->link->errno.")".$this->link-error);
		}
	}
	//eliminar datos
	public function delete($query){
			$delete_row=$this->link->query($query)or die($this->link->error._LINE_);
		if($delete_row){
			header("Location:listausuario.php?msg=".urlencode('Datos eliminados exitosamente!!!!'));
			exit();
		}else{
			die("Error:(".$this->link->errno.")".$this->error);
		}
	}
		public function deletepersonal($query){
			$delete_row=$this->link->query($query)or die($this->link->error._LINE_);
		if($delete_row){
			header("Location:listapersonal.php?msg=".urlencode('Datos eliminados exitosamente!!!!'));
			exit();
		}else{
			die("Error:(".$this->link->errno.")".$this->error);
		}
	}
	public function deleteusuario($query){
			$delete_row=$this->link->query($query)or die($this->link->error._LINE_);
		if($delete_row){
			header("Location:listausuario.php?msg=".urlencode('Datos eliminados exitosamente!!!!'));
			exit();
		}else{
			die("Error:(".$this->link->errno.")".$this->error);
		}
	}
		public function deleteinventario($query){
			$delete_row=$this->link->query($query)or die($this->link->error._LINE_);
		if($delete_row){
			header("Location:listainventario.php?msg=".urlencode('Datos eliminados exitosamente!!!!'));
			exit();
		}else{
			die("Error:(".$this->link->errno.")".$this->error);
		}
	}
			public function deletearticulo($query){
			$delete_row=$this->link->query($query)or die($this->link->error._LINE_);
		if($delete_row){
			header("Location:MostrarArticulo.php?msg=".urlencode('Datos eliminados exitosamente!!!!'));
			exit();
		}else{
			die("Error:(".$this->link->errno.")".$this->error);
		}
	}
	public function sigIn($query){
		$sign_row=$this->link->query($query) or die($this->link->error._LINE_);
			if($sign_row){
			header("Location:principal.php?msg=".urlencode('Datos correctos'));
			exit();
		}else{
			die("Error:(".$this->link->errno.")".$this->link-error);
		}
	}
		public function sigIn2($query){
		$sign_row=$this->link->query($query) or die($this->link->error._LINE_);
			if($sign_row){
			header("Location:principal1.php?msg=".urlencode('<h4>Datos correctos BIENVENIDO COLABORADOR!!!!<h4>'));
			exit();
		}else{
			die("Error:(".$this->link->errno.")".$this->link-error);
		}
	}
	public function register($query){
			$sign_row=$this->link->query($query) or die($this->link->error._LINE_);
			if($sign_row){
			header("Location:principal.php?msg=".urlencode('Datos registrados correctamente'));
			exit();
		}else{
			die("Error:(".$this->link->errno.")".$this->link-error);
		}
	}
}
?>