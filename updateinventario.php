<?php
	include 'sesion.php';//Autor: Lic. Marco Antonio dorado Goméz
	include "lib/config.php";
	include "lib/Database.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale=1.0, minimum-scale=1.0">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="shortcut icon" type="image/x-icon" href="img/ico.jpg">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/estilo.css">
		<title>Registro de Inventario</title>
</head>
<body>
	<section class="container">
		<?php
		$id= $_GET['id_prod'];
		$db= new Database();
		$query="SELECT * FROM tbl_inven WHERE id_prod=$id";
		$getData=$db->select($query)->fetch_assoc();

		if(isset($_POST['submit'])){
		
	
	$papel=mysqli_real_escape_string($db->link, $_POST['papel']);
	$gramaje=mysqli_real_escape_string($db->link, $_POST['gramaje']);
	$color=mysqli_real_escape_string($db->link, $_POST['color']);
	$canti=mysqli_real_escape_string($db->link, $_POST['canti']);
	$precio=mysqli_real_escape_string($db->link, $_POST['precio']);
	$foto=(isset($_FILES['foto']['name']))?$_FILES['foto']['name']:"";
		if($papel == '' || $gramaje == '' || $color == '' || $canti == '' || $precio == '' ||  $foto == ''){
			$error="Los campos no deben estar vacios!!!";
			}else{

				$fecha=new DateTime();
				$nomArchivo=($foto!="")?$fecha->getTimestamp()."_".$_FILES["foto"]["name"]:"avatar.png";
				$tmpfoto=$_FILES["foto"]["tmp_name"];
				if($tmpfoto !=""){
					move_uploaded_file($tmpfoto, "img/".$nomArchivo);
					$sentencia="SELECT * FROM tbl_inven WHERE id_prod=$id";
					$read=$db->select($query);

				}
				if(isset($nomArchivo["foto"])){
					if(file_exists("img/".$nomArchivo["foto"])){
						if($nomArchivo['foto']!="avatar.png"){
							unlink("img/".$nomArchivo["foto"]);
						}
					}

				}

		//$pass_cifrado = password_hash($contra, PASSWORD_DEFAULT);//encriptando la contraseña
		$query="UPDATE tbl_inven SET papel= '$papel', gramaje='$gramaje', color='$color', canti='$canti', precio='$precio',foto='$nomArchivo' WHERE id_prod=$id";
		$update = $db->updateinventario($query);
	}
}
?>
<?php
if(isset($_POST['delete'])){
	$query ="DELETE FROM tbl_inven WHERE id_prod=$id";
	$deleteData=$db->deleteinventario($query);
}
?>

		<div class="row my-5">
			<?php
			 if(isset($error)){
			 	echo"<div class='alert alert-danger'>".$error."</span></div>";
			 }
			 ?>
			<form class="login" action="updateinventario.php?id_prod=<?php echo $id;?>" method="POST" enctype="multipart/form-data">
				<h2><center>Actualizar Datos de Inventario</center></h2>
							 <div class="form-group">
			     <label type="papel" class="papel">Papel:(*)</label>
      				<select class="for-control" name="papel">
      				<option values="couche">COUCHE</option>
      				<option values="bond">BOND</option>
      				<option values="tornasol">TORNASOL</option>
      				<option values="cartulina hilada">CARTULINA HILADA</option>
      				<option values="papel craft">PAPEL CRAFT</option>
      				</select>
      			</div>
	      				<div class="form-group">
				<label class="text-info">Gramaje:(*)</label>
				<input type="text"  name="gramaje" id="gramaje" value="<?php echo $getData['gramaje'] ?>" placeholder="Introduzca Gramaje" class="form-control">
				</div>

					<div class="form-group">
			     <label type="color" class="color">Color:(*)</label>
      				<select class="for-control" name="color">
      				<option values="blanco">BLANCO</option>
      				<option values="crema">CREMA</option>
      				<option values="celeste">CELESTE</option>
      				<option values="marron">MARRON</option>
      				
      				</select>
      			</div>

				<div class="form-group">
				<label class="text-info">Cantidad:(*)</label>
				<input type="text"  name="canti" id="canti" value="<?php echo $getData['canti'] ?>" placeholder="Introduzca Cantidad" class="form-control">
				</div>


				<div class="form-group">
				<label class="text-info">Precio:(*)</label>
				<input type="number"  name="precio" id="precio" value="<?php echo $getData['precio'] ?>" placeholder="Introduzca Precio" class="form-control">
				</div>


					<div class="form-group">
					<label class="imagen"> Imagen:(*) </label>
					<input type="file" accept="image/*" name="foto" id="foto" value="" placeholder="" class="form-control">
				</div>


				<center>
					<button type="submit" name="submit" value="Update" class="btn btn-primary">Guardar</button>
					<button type="submit" name="delete" value="Delete" class="btn btn-danger">Eliminar</button>
					<a href="listaauto.php" class="btn btn-success">Cancelar</a><br><br>
					

				</center>
			</form>

		</div>
							<div class="col text-center"><!-- para que un boton este centrado -->

	<span><a class="btn btn-info btn-default btn-lg" href="principal.php">VOLVER A PRINCIPAL</a></span>
    </div>
	</section>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery-1.12.3.min.js"></script>
</body>
</html>