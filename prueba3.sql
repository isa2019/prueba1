-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-07-2019 a las 17:31:26
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prueba10`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_articulo`
--

CREATE TABLE `tbl_articulo` (
  `id_arti` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `foto` varchar(250) NOT NULL,
  `articulo` text NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_articulo`
--

INSERT INTO `tbl_articulo` (`id_arti`, `titulo`, `foto`, `articulo`, `fecha`) VALUES
(1, 'accidente en urujara', '1559138038_ser.ico', 'Articulos: Era un accidente fuertemente entre dos autos, hubo una doble colision.', '2019-05-29 13:53:58'),
(2, 'asfaltado en chijini', '1559141037_usuarios.ico', 'Articulos: Estan perjudicando el paso', '2019-05-29 14:43:57'),
(3, 'accidente autopista', '1559143312_3.jpg', 'Articulos: muy peligroso', '2019-05-29 15:21:52'),
(4, 'aaaaa', '1559143373_03.jpg', 'Articulos:fhaguifhauidfui', '2019-05-29 15:22:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_auto`
--

CREATE TABLE `tbl_auto` (
  `id_auto` int(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `color` varchar(100) NOT NULL,
  `modelo` varchar(50) NOT NULL,
  `placa` varchar(50) NOT NULL,
  `anio` varchar(50) NOT NULL,
  `chasis` varchar(50) NOT NULL,
  `bateria` varchar(50) NOT NULL,
  `motor` varchar(50) NOT NULL,
  `cale` varchar(50) NOT NULL,
  `precio` varchar(50) NOT NULL,
  `fecha_inicio` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_auto`
--

INSERT INTO `tbl_auto` (`id_auto`, `nombre`, `color`, `modelo`, `placa`, `anio`, `chasis`, `bateria`, `motor`, `cale`, `precio`, `fecha_inicio`) VALUES
(1, 'porsche carrera', 'verde', 'ford', '123-mdc', '1999', '123456', '123456', 'GASOLINA', 'SI', '3000', '2019-04-24 04:48:04'),
(2, 'porsche carrera', 'azul', 'toyota', '123-mdc', '1999', '123456', '123456', 'GASOLINA', 'NO', '3000', '2019-04-24 04:48:32'),
(4, 'vitarra', 'verde', 'ford', '12078', '1994', '7894567', 'toyo', 'ELECTRICO', 'NO', '100078', '2019-04-24 07:15:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_histo`
--

CREATE TABLE `tbl_histo` (
  `id_histo` int(11) NOT NULL,
  `estado` varchar(150) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_inven`
--

CREATE TABLE `tbl_inven` (
  `id_prod` int(11) NOT NULL,
  `papel` varchar(200) NOT NULL,
  `gramaje` varchar(200) NOT NULL,
  `color` varchar(200) NOT NULL,
  `canti` int(11) NOT NULL,
  `precio` int(11) NOT NULL,
  `foto` varchar(250) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_inven`
--

INSERT INTO `tbl_inven` (`id_prod`, `papel`, `gramaje`, `color`, `canti`, `precio`, `foto`, `fecha`) VALUES
(1, 'COUCHE', '73 grs.', 'BLANCO', 100, 500, '1559106418_bond.jpg', '2019-05-29 05:06:58'),
(2, 'COUCHE', '180 grs.', 'BLANCO', 1000, 250, '1559106501_1559106418_bond.jpg', '2019-05-29 05:08:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_login`
--

CREATE TABLE `tbl_login` (
  `id_login` int(100) NOT NULL,
  `user` int(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `f_inicio` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `foto` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_login`
--

INSERT INTO `tbl_login` (`id_login`, `user`, `password`, `f_inicio`, `foto`) VALUES
(7, 123456, '$2y$10$4vC44cWFMjOcMTPf38WqG.B.1J/QmNAWR33iy4qmnMqB1q8u96fxu', '2019-05-22 15:09:59', '1558504394_1557328134_isa1.jpg'),
(10, 789, '$2y$10$zgy6.AldBWAyaNOzO7Zu8OwpyugGjIrvRm7i6SM/aTNN45X0gcsNO', '2019-05-22 15:10:07', '1557932898_ico.jpg'),
(11, 456, '$2y$10$FYpE2xDGiIrnuslTJA.CuOn6rMXaRh9qL57Lni3faf/NavMTKoMvC', '2019-05-22 15:10:14', '1558504257_4.png'),
(12, 0, '$2y$10$XUoTjKgFVG9fW03U7Lp.juhPj1QZFhenM53GfzgX.w.wf807qFxtC', '2019-05-22 15:15:07', '1558538107_1557932898_ico.jpg'),
(13, 0, '$2y$10$GdlshktaN8oEBpZ6.fn1LuEAKpOc6RZ4GsxNzBYi2UsJ2kyFlm.8O', '2019-05-22 15:16:22', '1558538182_1558503799_4.png'),
(14, 123, '$2y$10$5ObC1UQwiC2v9mvFpB4iKe5C7dRgLtEGHRTwf2atgvIWd98h8FbEy', '2019-06-26 13:07:23', '1561554443_2.JPG');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_login2`
--

CREATE TABLE `tbl_login2` (
  `id_login` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `f_inicio` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fk_id_rol` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_login2`
--

INSERT INTO `tbl_login2` (`id_login`, `user`, `password`, `f_inicio`, `fk_id_rol`) VALUES
(1, 'isabel', '$2y$10$FYpE2xDGiIrnuslTJA.CuOn6rMXaRh9qL57Lni3faf/NavMTKoMvC', '2019-06-26 14:30:12', 1),
(2, 'tom', '$2y$10$FYpE2xDGiIrnuslTJA.CuOn6rMXaRh9qL57Lni3faf/NavMTKoMvC', '2019-06-26 14:32:15', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_personal`
--

CREATE TABLE `tbl_personal` (
  `id_p` int(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `cargo` varchar(100) NOT NULL,
  `salario` int(10) NOT NULL,
  `foto` varchar(250) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_personal`
--

INSERT INTO `tbl_personal` (`id_p`, `nombre`, `apellido`, `cargo`, `salario`, `foto`, `fecha`) VALUES
(1, 'isita', 'cat', 'SECRETARIA', 2500, '1558535140_04.jpg', '2019-05-22 14:25:40'),
(3, 'tomy', 'star', 'ADMINISTRADOR', 3500, '1558535877_isa1.jpg', '2019-05-22 14:37:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_rol`
--

CREATE TABLE `tbl_rol` (
  `idrol` int(1) NOT NULL,
  `rol` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_rol`
--

INSERT INTO `tbl_rol` (`idrol`, `rol`) VALUES
(1, 'administrador'),
(2, 'colaborador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `celular` int(20) NOT NULL,
  `especialidad` varchar(50) NOT NULL,
  `f_ingreso` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `nombre`, `direccion`, `celular`, `especialidad`, `f_ingreso`) VALUES
(1, 'cristina', 'calle 3 de mayo', 76545338, 'doctora', '2019-04-03 14:00:15'),
(2, 'tina', 'direccion', 0, '', '2019-04-03 14:23:12'),
(3, 'tom', 'direccion', 0, '', '2019-04-03 14:23:49'),
(4, 'tony', 'calle 35', 789, '', '2019-04-03 14:24:47'),
(5, 'marco', 'chijini 9', 777456123, '', '2019-04-03 14:26:17');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbl_articulo`
--
ALTER TABLE `tbl_articulo`
  ADD PRIMARY KEY (`id_arti`);

--
-- Indices de la tabla `tbl_auto`
--
ALTER TABLE `tbl_auto`
  ADD PRIMARY KEY (`id_auto`);

--
-- Indices de la tabla `tbl_histo`
--
ALTER TABLE `tbl_histo`
  ADD PRIMARY KEY (`id_histo`);

--
-- Indices de la tabla `tbl_inven`
--
ALTER TABLE `tbl_inven`
  ADD PRIMARY KEY (`id_prod`);

--
-- Indices de la tabla `tbl_login`
--
ALTER TABLE `tbl_login`
  ADD PRIMARY KEY (`id_login`);

--
-- Indices de la tabla `tbl_login2`
--
ALTER TABLE `tbl_login2`
  ADD PRIMARY KEY (`id_login`),
  ADD KEY `fk_id_rol` (`fk_id_rol`);

--
-- Indices de la tabla `tbl_personal`
--
ALTER TABLE `tbl_personal`
  ADD PRIMARY KEY (`id_p`);

--
-- Indices de la tabla `tbl_rol`
--
ALTER TABLE `tbl_rol`
  ADD PRIMARY KEY (`idrol`);

--
-- Indices de la tabla `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbl_articulo`
--
ALTER TABLE `tbl_articulo`
  MODIFY `id_arti` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tbl_auto`
--
ALTER TABLE `tbl_auto`
  MODIFY `id_auto` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tbl_inven`
--
ALTER TABLE `tbl_inven`
  MODIFY `id_prod` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbl_login`
--
ALTER TABLE `tbl_login`
  MODIFY `id_login` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `tbl_login2`
--
ALTER TABLE `tbl_login2`
  MODIFY `id_login` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tbl_personal`
--
ALTER TABLE `tbl_personal`
  MODIFY `id_p` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbl_rol`
--
ALTER TABLE `tbl_rol`
  MODIFY `idrol` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tbl_login2`
--
ALTER TABLE `tbl_login2`
  ADD CONSTRAINT `tbl_login2_ibfk_1` FOREIGN KEY (`fk_id_rol`) REFERENCES `tbl_rol` (`idrol`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
