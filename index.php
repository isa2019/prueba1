<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Sesion</title>
  <meta name="KEYWORDS" content="Sesion en linea template"> 
  <meta name="descripcion" content="pagina mejorada con php">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="shortcut icon" type="image/x-icon" href="img/ico.jpg">
	<link rel="stylesheet" type="text/css" href="css/formulario.css">


</head>
<body>

	<section class="container">
    <div class="row">
      <div class="col-sm-12">
        
      </div>
      <div class="col-sm-12">
      <div class="my-5"></div>
      <form class="login" action="checklogin.php" method="POST">
        <?php
        error_reporting(E_ALL ^ E_NOTICE);/*deja de mostrar notificaciones*/
        if($_GET["error"]=="si"){
          echo '<div class="alert alert-danger" role="alert"><center><strong>Ops!-Verifica tus datos.</strong></center></div>';
        }
        else{echo "";}
        ?>
        <h2><center>LOGUIN</center></h2>
        <div><img src="img/22.png" id="avatar"></div>

        <input  type="text" class="campo form-control" placeholder="Introduzca Usuario" name="user" id="user">
        <input  type="password" class="campo form-control" placeholder="Introduzca contraseña" name="pass" id="pass">

        <div>
          <label class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input">
            <span class="custom-control-indicador"></span>
            <span class="custom-control-description text-dark">Recordar contraseña en esta computadora</span>

          </label>
        </div>
        <button type="submit" name="submit" id="submit" class="btn btn-primary btn-lg btn-block">Iniciar Sesion</button>
        <span><a class="btn btn-danger btn-block btn-lg btn-block" href="index.php">Limpiar Datos</a></span>



      </form>
      
    </div>
</div>
	</section>	
 <script src="js/jquery-3.3.1.min.js"></script>
		  <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>


</body>
</html>