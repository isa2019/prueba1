-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-06-2019 a las 08:12:52
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prueba8`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_articulo`
--

CREATE TABLE `tbl_articulo` (
  `id_arti` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `foto` varchar(250) NOT NULL,
  `articulo` text NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_articulo`
--

INSERT INTO `tbl_articulo` (`id_arti`, `titulo`, `foto`, `articulo`, `fecha`) VALUES
(1, 'PASION POR TINTAS ', '1559713175_2a.jpg', 'Articulos: Este artÃ­culo fue hecho gracias al esfuerzo de trabajo de un periodista profesional, tenÃ­a su papÃ¡ que era un prensista. ', '2019-06-05 05:55:02'),
(3, 'IMPRENTA CLARIN', '1559713396_3a.jpg', 'Articulos: Trabajadores despedidos de la imprenta AGR-ClarÃ­n, marcharon desde el Obelisco hasta el Ministerio de Trabajo pidiendo justicia por sus despidos. ', '2019-06-05 05:56:01'),
(4, 'IMPRIME IMAGEN', '1559713574_4a.jpg', 'Articulos:El soporte de impresiÃ³n de cada elemento de una imagen corporativa puede variar segÃºn la calidad esperada, colores buena resoluciÃ³n.', '2019-06-05 06:00:10'),
(5, 'INVESTIGACION', '1559713745_5a.jpg', 'Articulos:El Ministerio PÃºblico abriÃ³ investigaciÃ³n disciplinaria contra Jaime Oswaldo Neira de la Torres, por presuntas irregularidades.', '2019-06-05 05:59:37'),
(7, 'LEY DE IMPRENTA', '1559713956_6a.jpg', 'Articulos:Los periodistas reafirmaron su compromiso de luchar hasta conseguir la derogaciÃ³n de los artÃ­culos que cuestionan del CÃ³digo Penal.', '2019-06-05 05:59:01'),
(8, 'VULNERABILIDAD', '1559714320_8A.jpg', 'Articulos:La abogada Paola Cortes advirtiÃ³ este viernes sobre tres artÃ­culos del CÃ³digo del Sistema Penal que vulneran la Ley de Imprenta.', '2019-06-05 05:58:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_auto`
--

CREATE TABLE `tbl_auto` (
  `id_auto` int(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `color` varchar(100) NOT NULL,
  `modelo` varchar(50) NOT NULL,
  `placa` varchar(50) NOT NULL,
  `anio` varchar(50) NOT NULL,
  `chasis` varchar(50) NOT NULL,
  `bateria` varchar(50) NOT NULL,
  `motor` varchar(50) NOT NULL,
  `cale` varchar(50) NOT NULL,
  `precio` varchar(50) NOT NULL,
  `fecha_inicio` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_auto`
--

INSERT INTO `tbl_auto` (`id_auto`, `nombre`, `color`, `modelo`, `placa`, `anio`, `chasis`, `bateria`, `motor`, `cale`, `precio`, `fecha_inicio`) VALUES
(1, 'porsche carrera', 'verde', 'ford', '123-mdc', '1999', '123456', '123456', 'GASOLINA', 'SI', '3000', '2019-04-24 04:48:04'),
(2, 'porsche carrera', 'azul', 'toyota', '123-mdc', '1999', '123456', '123456', 'GASOLINA', 'NO', '3000', '2019-04-24 04:48:32'),
(4, 'vitarra', 'verde', 'ford', '12078', '1994', '7894567', 'toyo', 'ELECTRICO', 'NO', '100078', '2019-04-24 07:15:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_histo`
--

CREATE TABLE `tbl_histo` (
  `id_histo` int(11) NOT NULL,
  `estado` varchar(150) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_inven`
--

CREATE TABLE `tbl_inven` (
  `id_prod` int(11) NOT NULL,
  `papel` varchar(200) NOT NULL,
  `gramaje` varchar(200) NOT NULL,
  `color` varchar(200) NOT NULL,
  `canti` int(11) NOT NULL,
  `precio` int(11) NOT NULL,
  `foto` varchar(250) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_inven`
--

INSERT INTO `tbl_inven` (`id_prod`, `papel`, `gramaje`, `color`, `canti`, `precio`, `foto`, `fecha`) VALUES
(1, 'COUCHE', '73 grs.', 'BLANCO', 100, 500, '1559106418_bond.jpg', '2019-05-29 05:06:58'),
(2, 'COUCHE', '180 grs.', 'BLANCO', 1000, 250, '1559106501_1559106418_bond.jpg', '2019-05-29 05:08:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_login`
--

CREATE TABLE `tbl_login` (
  `id_login` int(100) NOT NULL,
  `user` int(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `f_inicio` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `foto` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_login`
--

INSERT INTO `tbl_login` (`id_login`, `user`, `password`, `f_inicio`, `foto`) VALUES
(7, 123456, '$2y$10$4vC44cWFMjOcMTPf38WqG.B.1J/QmNAWR33iy4qmnMqB1q8u96fxu', '2019-05-22 15:09:59', '1558504394_1557328134_isa1.jpg'),
(10, 789, '$2y$10$zgy6.AldBWAyaNOzO7Zu8OwpyugGjIrvRm7i6SM/aTNN45X0gcsNO', '2019-05-22 15:10:07', '1557932898_ico.jpg'),
(11, 456, '$2y$10$FYpE2xDGiIrnuslTJA.CuOn6rMXaRh9qL57Lni3faf/NavMTKoMvC', '2019-05-22 15:10:14', '1558504257_4.png'),
(12, 0, '$2y$10$XUoTjKgFVG9fW03U7Lp.juhPj1QZFhenM53GfzgX.w.wf807qFxtC', '2019-05-22 15:15:07', '1558538107_1557932898_ico.jpg'),
(13, 0, '$2y$10$GdlshktaN8oEBpZ6.fn1LuEAKpOc6RZ4GsxNzBYi2UsJ2kyFlm.8O', '2019-05-22 15:16:22', '1558538182_1558503799_4.png'),
(14, 123, '$2y$10$W48VQxzXKay1efeG7pz3NeN2B8F46nQn9p4Hy.1lcXhbUQ.hdFneG', '2019-05-22 15:16:45', '1558538205_1557932640_login2.png'),
(15, 789, '$2y$10$SmBQ4O1EJQCTZnfUGWfDR.bIvzNML7be8TjkAHoGwcrwlxejcKV5i', '2019-05-22 15:19:27', '1558538367_login2.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_personal`
--

CREATE TABLE `tbl_personal` (
  `id_p` int(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `cargo` varchar(100) NOT NULL,
  `salario` int(10) NOT NULL,
  `foto` varchar(250) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_personal`
--

INSERT INTO `tbl_personal` (`id_p`, `nombre`, `apellido`, `cargo`, `salario`, `foto`, `fecha`) VALUES
(1, 'isita', 'cat', 'SECRETARIA', 2500, '1558535140_04.jpg', '2019-05-22 14:25:40'),
(3, 'tomy', 'star', 'ADMINISTRADOR', 3500, '1558535877_isa1.jpg', '2019-05-22 14:37:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(100) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `celular` int(20) NOT NULL,
  `especialidad` varchar(50) NOT NULL,
  `f_ingreso` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `nombre`, `direccion`, `celular`, `especialidad`, `f_ingreso`) VALUES
(1, 'cristina', 'calle 3 de mayo', 76545338, 'doctora', '2019-04-03 14:00:15'),
(2, 'tina', 'direccion', 0, '', '2019-04-03 14:23:12'),
(3, 'tom', 'direccion', 0, '', '2019-04-03 14:23:49'),
(4, 'tony', 'calle 35', 789, '', '2019-04-03 14:24:47'),
(5, 'marco', 'chijini 9', 777456123, '', '2019-04-03 14:26:17');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbl_articulo`
--
ALTER TABLE `tbl_articulo`
  ADD PRIMARY KEY (`id_arti`);

--
-- Indices de la tabla `tbl_auto`
--
ALTER TABLE `tbl_auto`
  ADD PRIMARY KEY (`id_auto`);

--
-- Indices de la tabla `tbl_histo`
--
ALTER TABLE `tbl_histo`
  ADD PRIMARY KEY (`id_histo`);

--
-- Indices de la tabla `tbl_inven`
--
ALTER TABLE `tbl_inven`
  ADD PRIMARY KEY (`id_prod`);

--
-- Indices de la tabla `tbl_login`
--
ALTER TABLE `tbl_login`
  ADD PRIMARY KEY (`id_login`);

--
-- Indices de la tabla `tbl_personal`
--
ALTER TABLE `tbl_personal`
  ADD PRIMARY KEY (`id_p`);

--
-- Indices de la tabla `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbl_articulo`
--
ALTER TABLE `tbl_articulo`
  MODIFY `id_arti` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `tbl_auto`
--
ALTER TABLE `tbl_auto`
  MODIFY `id_auto` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `tbl_inven`
--
ALTER TABLE `tbl_inven`
  MODIFY `id_prod` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tbl_login`
--
ALTER TABLE `tbl_login`
  MODIFY `id_login` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `tbl_personal`
--
ALTER TABLE `tbl_personal`
  MODIFY `id_p` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
