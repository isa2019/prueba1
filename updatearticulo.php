<?php
	include 'sesion.php';//Autor: Lic. Marco Antonio dorado Goméz
	include "lib/config.php";
	include "lib/Database.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale=1.0, minimum-scale=1.0">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="shortcut icon" type="image/x-icon" href="img/ico.jpg">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/estilo.css">
		 <script src="js/validper.js"></script>
		<title>Registro de Articulo</title>
</head>
<body>
	<section class="container">
		

			<?php
		$id= $_GET['id_arti'];
		$db= new Database();
		$query="SELECT * FROM tbl_articulo WHERE id_arti=$id";
		$getData=$db->select($query)->fetch_assoc();

		if(isset($_POST['submit'])){
		
	
	$titulo=mysqli_real_escape_string($db->link, $_POST['titulo']);
	
	$foto=(isset($_FILES['foto']['name']))?$_FILES['foto']['name']:"";
	$articulo=mysqli_real_escape_string($db->link, $_POST['articulo']);
	
		if($titulo == '' || $articulo == ''){
			$error="Los campos no deben estar vacios!!!";
			}else{

				$fecha=new DateTime();
				

				if($foto!='')
				{
							$nomArchivo=($foto!="")?$fecha->getTimestamp()."_".$_FILES["foto"]["name"]:"avatar.png";
							$tmpfoto=$_FILES["foto"]["tmp_name"];
							if($tmpfoto !=""){
								move_uploaded_file($tmpfoto, "img/".$nomArchivo);
								$sentencia="SELECT * FROM tbl_articulo WHERE id_arti=$id";
								$read=$db->select($query);

							}


							if(isset($nomArchivo["foto"])){
								if(file_exists("img/".$nomArchivo["foto"])){
									if($nomArchivo['foto']!="avatar.png"){
										unlink("img/".$nomArchivo["foto"]);
									}
								}

							}
				}


				
				
		//$pass_cifrado = password_hash($contra, PASSWORD_DEFAULT);//encriptando la contraseña
		if($foto!='')
		{
			$query="UPDATE tbl_articulo SET titulo= '$titulo', foto='$nomArchivo',articulo='$articulo' WHERE id_arti='$id'";
		}
		else
		{
		$query="UPDATE tbl_articulo SET titulo= '$titulo', articulo='$articulo' WHERE id_arti='$id'";
		}


		$update = $db->updatearticulo($query);
	}
}
?>
<?php
if(isset($_POST['delete'])){
	$query ="DELETE FROM tbl_inven WHERE id_prod=$id";
	$deleteData=$db->deleteinventario($query);
}
?>

		<div class="row my-5">
			<?php
			 if(isset($error)){
			 	echo"<div class='alert alert-danger'>".$error."</span></div>";
			 }
			 ?>
			<form class="login" action="updatearticulo.php?id_arti=<?php echo $id;?>" method="POST" enctype="multipart/form-data">
				<h2><center>Actualizar Datos de Articulo</center></h2>
				<div class="form-group">
				<label class="text-info">Titulo:(*)</label>
				<input type="text"  name="titulo" id="titulo" value="<?php echo $getData['titulo'] ?>" placeholder="Introduzca Titulo" class="form-control">
				</div>
						<div class="form-group">
					<label class="imagen"> Imagen:(*) </label>
					<input type="file" accept="image/*" name="foto" id="foto" value="" placeholder="" class="form-control">
				</div>
				<textarea name="articulo"  rows="10" cols="45"><?php echo $getData['articulo'] ?></textarea>
					


				<center>
					<button type="submit" name="submit" value="Update" class="btn btn-primary">Guardar</button>
					<button type="submit" name="delete" value="Delete" class="btn btn-danger">Eliminar</button>
					<a href="listaauto.php" class="btn btn-success">Cancelar</a><br><br>
					

				</center>
			</form>

		</div>
							<div class="col text-center"><!-- para que un boton este centrado -->

	<span><a class="btn btn-info btn-default btn-lg" href="principal.php">VOLVER A PRINCIPAL</a></span>
    </div>
	</section>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery-1.12.3.min.js"></script>
</body>
</html>