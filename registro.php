<?php
	include 'sesion.php';//Autor: Lic. Marco Antonio dorado Goméz
	include "lib/config.php";
	include "lib/Database.php";
?>
<?php
	$db=new Database();
	if(isset($_POST['submit'])){
	$usuario=mysqli_real_escape_string($db->link, $_POST['user']);
	$contra=mysqli_real_escape_string($db->link, $_POST['pass']);
	$foto=(isset($_FILES['foto']['name']))?$_FILES['foto']['name']:"";
	if($usuario == '' || $contra == '' || $foto == ''){
		$error="Los campos no deben estar vacios!!!";
	}else{
		$fecha= new DateTime();
		$nomArchivo=($foto!="")?$fecha->getTimestamp()."_".$_FILES["foto"]["name"]:"avatar.png";
		$tmpfoto=$_FILES["foto"]["tmp_name"];

		$pass_cifrado = password_hash($contra, PASSWORD_DEFAULT);//encriptando la contraseña
		$query="INSERT INTO tbl_login(user, password,foto) Values('$usuario','$pass_cifrado','$nomArchivo')";
		if($tmpfoto !=""){
			move_uploaded_file($tmpfoto, "img/".$nomArchivo);
		}
		$create = $db->insert($query);
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale=1.0, minimum-scale=1.0">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="shortcut icon" type="image/x-icon" href="img/ico.jpg">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/estilo.css">
		<title>Registro de Usuarios</title>

</head>
<body>
	<section class="container">
		<div class="row my-5">
			<form class="login" action="registro.php" method="POST" enctype="multipart/form-data">
				<?php
				if(isset($error)){
					echo"<center><div class='alert alert-danger'><span>".$error."</span></div></center>";

				}
				?>
				<h2><center>Registra Usuario</center></h2>
				<input type="text" class="campo" placeholder="Registrar Usuario" name="user" id="user">
				<input type="password" class="campo" placeholder="Registrar contraseña" name="pass" id="pass">
				
				<div class="form-group">
					<label class="imagen"> Imagen:(*) </label>
					<input type="file" accept="image/*" name="foto" id="foto" value="" placeholder="" class="form-control">


					
				</div>
					<button type="submit" name="submit" id="submit" class="btn btn-primary">Registrar</button>
					<button type="reset" value="Cancel" class="btn btn-success">Limpiar Datos</button>
					<a href="principal.php" class="btn btn-danger">Cancelar</a><br><br>
					

				</center>
			</form>

		</div>
							<div class="col text-center"><!-- para que un boton este centrado -->

	<span><a class="btn btn-secondary btn-default btn-lg" href="principal.php">VOLVER A PRINCIPAL</a></span>
    </div>
	</section>
	<script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap.bundle.min.js"></script>
</body>
</html>