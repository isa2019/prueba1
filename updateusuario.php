<?php
	include 'sesion.php';//Autor: Lic. Marco Antonio dorado Goméz
	include "lib/config.php";
	include "lib/Database.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale=1.0, minimum-scale=1.0">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="shortcut icon" type="image/x-icon" href="img/ico.jpg">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/estilo.css">
		<title>Registro de Auto</title>
</head>
<body>
	<section class="container">
		<?php
		$id= $_GET['id_login'];
		$db= new Database();
		$query="SELECT * FROM tbl_login WHERE id_login=$id";
		$getData=$db->select($query)->fetch_assoc();

		if(isset($_POST['submit'])){

		$user=mysqli_real_escape_string($db->link, $_POST['user']);// para dar seguridad al enviar consulta Mysql
		$password=mysqli_real_escape_string($db->link, $_POST['password']);
		$foto=(isset($_FILES['foto']['name']))?$_FILES['foto']['name']:"";
		
		if($user == '' || $password == '' || $foto == ''){

			$error="Los campos no deben estar vacios!!!";
			}else {

				$fecha=new DateTime();
				$nomArchivo=($foto!="")?$fecha->getTimestamp()."_".$_FILES["foto"]["name"]:"avatar.png";
				$tmpfoto=$_FILES["foto"]["tmp_name"];
				if($tmpfoto !=""){
					move_uploaded_file($tmpfoto, "img/".$nomArchivo);
					$sentencia="SELECT * FROM tb_login WHERE id=$id";
					$read=$db->select($query);

				}
				if(isset($nomArchivo["foto"])){
					if(file_exists("img/".$nomArchivo["foto"])){
						if($nomArchivo['foto']!="avatar.png"){
							unlink("img/".$nomArchivo["foto"]);
						}
					}

				}
			

		//$pass_cifrado = password_hash($contra, PASSWORD_DEFAULT);//encriptando la contraseña
		$pass_cifrado = password_hash($password, PASSWORD_DEFAULT);				
		$query="UPDATE tbl_login SET user= '$user', password='$pass_cifrado',foto='$nomArchivo' WHERE id_login=$id";
		$update = $db->update($query);
	}
}
?>
<?php
if(isset($_POST['delete'])){
	$query ="DELETE FROM tbl_login WHERE id_login=$id";
	$deleteData=$db->delete($query);
}
?>

		<div class="row my-5">
			<?php
			 if(isset($error)){
			 	echo"<div class='alert alert-danger'>".$error."</span></div>";
			 }
			 ?>
			<form class="login" action="updateusuario.php?id_login=<?php echo $id;?>" method="POST" enctype="multipart/form-data">
				<h2><center>Actualizar Datos de Usuario</center></h2>
				<div class="form-group">
				<label class="text-info">Usuario:(*)</label>
				<input type="text" maxlength="50" name="user" id="user" value="<?php echo $getData['user'] ?>" placeholder="Introduzca Usuario" class="form-control">
				</div>
				<div class="form-group">
				<label class="text-info">Password:(*)</label>
				<input type="password" maxlength="50" name="password" id="password" value="<?php echo $getData['password'] ?>" placeholder="Introduzca Password" class="form-control">
				</div>
				<div class="form-group">
				 <td><center><img class="img-thumbnail" width="100px" src="img/<?php echo $getData['foto'];?>"></td></center>
				<label class="text-info">Imagen:(*)</label>
				<input type="file" accept="image/*" name="foto" id="foto" value="" placeholder="" class="form-control">
				
				</div>
		
				<center>
					<button type="submit" name="submit" value="Update" class="btn btn-primary">Guardar</button>
					<button type="submit" name="delete" value="Delete" class="btn btn-danger">Eliminar</button>
					<a href="listausuario.php" class="btn btn-success">Cancelar</a><br><br>
					

				</center>
			</form>

		</div>
							<div class="col text-center"><!-- para que un boton este centrado -->

	<span><a class="btn btn-info btn-default btn-lg" href="principal.php">VOLVER A PRINCIPAL</a></span>
    </div>
	</section>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery-1.12.3.min.js"></script>
</body>
</html>