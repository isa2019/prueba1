<?php
	include 'sesion.php';//Autor: Lic. Marco Antonio dorado Goméz
	include "lib/config.php";
	include "lib/Database.php";
?>
<?php
	$db=new Database();
	if(isset($_POST['submit'])){
	$nombre=mysqli_real_escape_string($db->link, $_POST['nombre']);
	$apellido=mysqli_real_escape_string($db->link, $_POST['apellido']);
	$cargo=mysqli_real_escape_string($db->link, $_POST['cargo']);
	$salario=mysqli_real_escape_string($db->link, $_POST['salario']);
	$foto=(isset($_FILES['foto']['name']))?$_FILES['foto']['name']:"";
	
	
	if($nombre == '' || $apellido == '' || $cargo == '' || $salario == '' || $foto == ''){
		$error="Los campos no deben estar vacios!!!";
	}else{
		$fecha= new DateTime();
		$nomArchivo=($foto!="")?$fecha->getTimestamp()."_".$_FILES["foto"]["name"]:"avatar.png";
		$tmpfoto=$_FILES["foto"]["tmp_name"];
		//$pass_cifrado = password_hash($contra, PASSWORD_DEFAULT);//encriptando la contraseña
		$query="INSERT INTO tbl_personal(nombre,apellido,cargo,salario,foto) Values('$nombre','$apellido','$cargo','$salario','$nomArchivo')";
			if($tmpfoto !=""){
			move_uploaded_file($tmpfoto, "img/".$nomArchivo);
		}

		$create = $db->insertpersonal($query);
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale=1.0, minimum-scale=1.0">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="shortcut icon" type="image/x-icon" href="img/ico.jpg">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/estilo.css">
		 <script src="js/validper.js"></script>
		<title>Registro de Personal</title>
</head>
<body>
	<section class="container">
		<div class="row my-5">
			<form class="login" action="personal.php" method="POST" enctype="multipart/form-data" name="formularito" onsubmit="return ValidaDato()">
				<?php
				if(isset($error)){
					echo"<center><div class='alert alert-danger'><span>".$error."</span></div></center>";

				}
				?>
				<h2><center>Registrar Personal</center></h2>
				<input type="text" class="campo" placeholder="Ingresar Nombre" name="nombre" id="nombre">
				<input type="text" class="campo" placeholder="Ingresar Apellido" name="apellido" id="apellido">
				 <div class="form-group">
			     <label type="cargo" class="cargo">cargo:</label>
      				<select class="for-control" name="cargo">
      				<option values="secretaria">SECRETARIA</option>
      				<option values="administrador">ADMINISTRADOR</option>
      				<option values="auxiliar">AUXLIAR</option>
      				</select>
      			</div>

      			<div class="form-group">
			     <label type="salario" class="salario">SALARIO:</label>
      				<select class="for-control" name="salario">
      				<option values="basico">1500</option>
      				<option values="minimo">2500</option>
      				<option values="regular">3500</option>
      			    </select>
      			</div>
				
				
					<div class="form-group">
					<label class="imagen"> Imagen:(*) </label>
					<input type="file" accept="image/*" name="foto" id="foto" value="" placeholder="" class="form-control">
				</div>
				
				
				
		
      			

				<center>
					<button type="submit" name="submit" id="submit" class="btn btn-primary">Registrar</button>
					<button type="reset" value="Cancel" class="btn btn-success">Limpiar Datos</button>
					<a href="principal.php" class="btn btn-danger">Cancelar</a>
				</center>
			</form>
		</div>
					<div class="col text-center"><!-- para que un boton este centrado -->

	<span><a class="btn btn-secondary btn-default btn-lg" href="principal.php">VOLVER A PRINCIPAL</a></span>
    </div>
		</div>


	</section>
	<script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

</body>
</html>