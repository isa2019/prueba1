<?php
	include 'sesion.php';//Autor: Lic. Marco Antonio dorado Goméz
	include "lib/config.php";
	include "lib/Database.php";
?>
<?php
	$db=new Database();
	if(isset($_POST['submit'])){
	$producto=mysqli_real_escape_string($db->link, $_POST['producto']);
	$medida=mysqli_real_escape_string($db->link, $_POST['medida']);
	$unidad_medida=mysqli_real_escape_string($db->link, $_POST['unidad_medida']);
	$unidad_max=mysqli_real_escape_string($db->link, $_POST['unidad_max']);
	$unidad_min=mysqli_real_escape_string($db->link, $_POST['unidad_min']);
	$stock=mysqli_real_escape_string($db->link, $_POST['stock']);
	$foto=(isset($_FILES['foto']['name']))?$_FILES['foto']['name']:"";
	
	
	if($producto == '' || $medida == '' || $unidad_medida == '' || $unidad_max == '' || $unidad_min == '' || $stock == '' || $foto == ''){
		$error="Los campos no deben estar vacios!!!";
	}else{
		$fecha= new DateTime();
		$nomArchivo=($foto!="")?$fecha->getTimestamp()."_".$_FILES["foto"]["name"]:"avatar.png";
		$tmpfoto=$_FILES["foto"]["tmp_name"];
		//$pass_cifrado = password_hash($contra, PASSWORD_DEFAULT);//encriptando la contraseña
		$query="INSERT INTO tbl_inven(producto,medida,unidad_medida,unidad_max,unidad_min,stock,foto) Values('$producto','$medida','$unidad_medida','$unidad_max','$unidad_min','$stock','$nomArchivo')";
			if($tmpfoto !=""){
			move_uploaded_file($tmpfoto, "img/".$nomArchivo);
		}

		$create = $db->insertinventario($query);
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale=1.0, minimum-scale=1.0">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="shortcut icon" type="image/x-icon" href="img/ico.jpg">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/estilo.css">
		 <script src="js/validper.js"></script>
		<title>Registro de Inventario</title>
</head>
<body>
	<section class="container">
		<div class="row my-5">
			<form class="login" action="inventario.php" method="POST" enctype="multipart/form-data" name="formularito" onsubmit="return ValidaDato()">
				<?php
				if(isset($error)){
					echo"<center><div class='alert alert-danger'><span>".$error."</span></div></center>";

				}
				?>
				<h2><center>Registrar Inventario</center></h2>
			
				<input type="text" class="campo" placeholder="Ingresar Producto" name="producto" id="producto">
				<input type="number" class="campo" placeholder="Ingresar Medida" name="medida" id="medida">
				<input type="text" class="campo" placeholder="Ingresar Unidad de Medida" name="unidad_medida" id="unidad_medida">
				<input type="number" class="campo" placeholder="Ingresar Unidad Máxima" name="unidad_max" id="unidad_max">
				<input type="number" class="campo" placeholder="Ingresar Unidad Mínima" name="unidad_min" id="unidad_min">
				<input type="number" class="campo" placeholder="Ingresar Stock" name="stock" id="stock">
				<div class="form-group">
					<label class="imagen"> Imagen:(*) </label>
					<input type="file" accept="image/*" name="foto" id="foto" value="" placeholder="" class="form-control">
				</div>
				
				
				
		
      			

				<center>
					<button type="submit" name="submit" id="submit" class="btn btn-primary">Registrar</button>
					<button type="reset" value="Cancel" class="btn btn-success">Limpiar Datos</button>
					<a href="principal.php" class="btn btn-danger">Cancelar</a>
				</center>
			</form>
		</div>
					<div class="col text-center"><!-- para que un boton este centrado -->

	<span><a class="btn btn-secondary btn-default btn-lg" href="principal.php">VOLVER A PRINCIPAL</a></span>
    </div>
		</div>


	</section>
	<script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

</body>
</html>