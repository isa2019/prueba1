<?php
	include 'sesion.php';//Autor: Lic. Marco Antonio dorado Goméz
	include "lib/config.php";
	include "lib/Database.php";
?>
<?php
	$db=new Database();
	if(isset($_POST['submit'])){
	$papel=mysqli_real_escape_string($db->link, $_POST['papel']);
	$gramaje=mysqli_real_escape_string($db->link, $_POST['gramaje']);
	$color=mysqli_real_escape_string($db->link, $_POST['color']);
	$canti=mysqli_real_escape_string($db->link, $_POST['canti']);
	$precio=mysqli_real_escape_string($db->link, $_POST['precio']);
	$foto=(isset($_FILES['foto']['name']))?$_FILES['foto']['name']:"";
	
	
	if($papel == '' || $gramaje == '' || $color == '' || $canti == '' || $precio == ''  || $foto == ''){
		$error="Los campos no deben estar vacios!!!";
	}else{
		$fecha= new DateTime();
		$nomArchivo=($foto!="")?$fecha->getTimestamp()."_".$_FILES["foto"]["name"]:"avatar.png";
		$tmpfoto=$_FILES["foto"]["tmp_name"];
		//$pass_cifrado = password_hash($contra, PASSWORD_DEFAULT);//encriptando la contraseña
		$query="INSERT INTO tbl_inven(papel,gramaje,color,canti,precio,foto) Values('$papel','$gramaje','$color','$canti','$precio','$nomArchivo')";
			if($tmpfoto !=""){
			move_uploaded_file($tmpfoto, "img/".$nomArchivo);
		}

		$create = $db->insertinventario($query);
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale=1.0, minimum-scale=1.0">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="shortcut icon" type="image/x-icon" href="img/ico.jpg">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/estilo.css">
		 <script src="js/validper.js"></script>
		<title>Registro de Inventario de Imprenta</title>
</head>
<body>
	<section class="container">
		<div class="row my-5">
			<form class="login" action="inventario.php" method="POST" enctype="multipart/form-data" name="formularito" onsubmit="return ValidaDato()">
				<?php
				if(isset($error)){
					echo"<center><div class='alert alert-danger'><span>".$error."</span></div></center>";

				}
				?>
				<h2><center>Registro de Inventario de Imprenta</center></h2>
						 <div class="form-group">
			     <label type="papel" class="papel">Papel:</label>
      				<select class="for-control" name="papel">
      				<option values="couche">COUCHE</option>
      				<option values="bond">BOND</option>
      				<option values="tornasol">TORNASOL</option>
      				<option values="cartulina hilada">CARTULINA HILADA</option>
      				<option values="papel craft">PAPEL CRAFT</option>
      				</select>
      			</div>
      		<input type="text" class="campo" placeholder="Ingresar Gramaje" name="gramaje" id="gramaje">
      					 <div class="form-group">
			     <label type="color" class="color">Color:</label>
      				<select class="for-control" name="color">
      				<option values="blanco">BLANCO</option>
      				<option values="crema">CREMA</option>
      				<option values="celeste">CELESTE</option>
      				<option values="marron">MARRON</option>
      				
      				</select>
      			</div>
			
				<input type="number" class="campo" placeholder="Ingresar Cantidad" name="canti" id="canti">
				<input type="number" class="campo" placeholder="Ingresar Precio" name="precio" id="precio">
				<div class="form-group">
					<label class="imagen"> Imagen:(*) </label>
					<input type="file" accept="image/*" name="foto" id="foto" value="" placeholder="" class="form-control">
				</div>
				
				
				
		
      			

				<center>
					<button type="submit" name="submit" id="submit" class="btn btn-primary">Registrar</button>
					<button type="reset" value="Cancel" class="btn btn-success">Limpiar Datos</button>
					<a href="principal.php" class="btn btn-danger">Cancelar</a>
				</center>
			</form>
		</div>
					<div class="col text-center"><!-- para que un boton este centrado -->

	<span><a class="btn btn-secondary btn-default btn-lg" href="principal.php">VOLVER A PRINCIPAL</a></span>
    </div>
		</div>


	</section>
	<script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

</body>
</html>