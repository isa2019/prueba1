<?php
include 'sesion.php';
include "lib/config.php";
include "lib/Database.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Listado de Inventario</title>
  <meta name="KEYWORDS" content="Sesion en linea template"> 
  <meta name="descripcion" content="pagina mejorada con php">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="shortcut icon" type="image/x-icon" href="img/ico.jpg">
	<link rel="stylesheet" type="text/css" href="css/fondito.css">


</head>
<body>

	<section class="container">
    <div class="row">
      <div class="col-sm-12">
        <?php
        $db= new Database();
        $query="SELECT * FROM tbl_inven";
        $read = $db->select($query);
        ?>
        <?php
        if(isset($_GET['msg'])){
        echo "<div class='alert alert-primary'><span>".$_GET['msg']."</span></div>"; 
      } 
      ?>
      </div>
      <div class="col-sm-12">
        <table class="table table-hover">
          <thead class="thead-dark">
            <tr>
              <th scope="col">id_papel</th>
              <th scope="col">Nombre de Papel</th>
              <th scope="col">Gramaje</th>
              <th scope="col">Color</th>
              <th scope="col">Cantidad</th>
              <th scope="col">Precio</th>
              <th scope="col">Fecha de Ingreso al sistema</th>
              <th scope="col">Foto</th>
              <th scope="col">Editar</th>
              <th scope="col">Eliminar</th>
            </tr>
          </thead>
          <?php if($read){?>
          <?php
          $i=1;
          while($row=$read->fetch_assoc()){
          ?>
          <tbody>
            <tr>
              <td><?php echo $row['id_prod'];?></td>
              <td><?php echo $row['papel'];?></td>
              <td><?php echo $row['gramaje'];?></td>
              <td><?php echo $row['color'];?></td>
              <td><?php echo $row['canti'];?></td>
              <td><?php echo $row['precio'];?></td>
              <td><?php echo $row['fecha'];?></td>
              <td><img class="img-thumbnail" width="100px" src="img/<?php echo $row['foto'];?>"></td>
              <td><a href="updateinventario.php?id_prod=<?php echo urlencode($row['id_prod']); ?>" class="btn btn-primary btn-sm">Editar</a></td>
                <td><a href="deleteinventario.php?delete=1&id_prod=<?php echo urlencode($row['id_prod']); ?>" class="btn btn-primary btn-sm">Eliminar</a></td>
            </tr>
          </tbody>
            <?php } ?>
            <?php } else { ?>
            <p> Los datos no son validos!!</p>
            <?php } ?>
        </table>
        <div class="form-group">
          <label>
            <span><a href="principal.php" class="btn btn-secondary">Ir a principal</a></span>
            <span><a href="logout.php" class="btn btn-warning">Salir de sistema</a></span>
            <span><a href="personal.php" class="btn btn-secondary">Registrar Personal</a></span>
            <span><a href="inventario.php" class="btn btn-warning">Registrar Inventario</a></span>
             <span><a href="registro.php" class="btn btn-secondary">Registrar Nuevo Usuario</a></span>
                <span><a href="index_word.php" class="btn btn-warning">Exportar a Word</a></span>
                <span><a href="index_pdf.php" class="btn btn-secondary">Exportar a Pdf</a></span>
          </label>
          
      </div>
  
    </div>
</div>
	</section>	
 <script src="js/jquery-3.3.1.min.js"></script>
 <script src="js/bootstrap.min.js"></script>
    <!--<script src="js/bootstrap.bundle.min.js"></script>-->


</body>
</html>