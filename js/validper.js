	function ValidaDato()
{
  /*Instanciamos lasvariables*/
 var nombre = document.formularito.nombre;
 var color = document.formularito.color;
 var modelo=document.formularito.modelo;
 var placa=document.formularito.placa;
  var anio=document.formularito.anio;
 var chasis=document.formularito.chasis;
 var bateria=document.formularito.bateria;
  var precio=document.formularito.precio;
 

/*--validando nombre---------------------------------------------------------*/
  if(nombre.value=="")
  {
    alert("LLENE EL CAMPO NOMBRE")
    return false;
  } 
  for(var i=0;i<nombre.value.length; i++){
    if(isNaN(parseInt(nombre.value.charAt(i)))!=true)
    {
      alert("SOLAMENTE SE PERMITEN LETRAS EN CAMPO NOMBRE");
      return false;
    }
  }
  if(nombre.value.length>20)
  {   
    alert("LOS CARACTERES DE CAMPO NOMBRE EXCEDEN LIMITE DE LETRAS, DEBE SER MENOR A 40 LETRAS");
    return false;
  }
  if(nombre.value.length<3)
  {   
    alert("EL CAMPO NOMBRE DEBE SER POR LO MENOS MAYOR A 3 LETRAS");
    return false;
  }

/*--validando color---------------------------------------------------------*/
  if(color.value=="")
  {
    alert("LLENE EL CAMPO COLOR")
    return false;
  } 
  for(var i=0;i<color.value.length; i++){
    if(isNaN(parseInt(color.value.charAt(i)))!=true)
    {
      alert("SOLAMENTE SE PERMITEN LETRAS EN CAMPO COLOR");
      return false;
    }
  }
  if(color.value.length>20)
  {   
    alert("LAS LETRAS DE CAMPO COLOR EXCEDEN LIMITE DE LETRAS, DEBE SER MENOR A 20 LETRAS");
    return false;
  }
  if(color.value.length<3)
  {   
    alert("EL CAMPO COLOR DEBE SER POR LO MENOS MAYOR A 3 LETRAS");
    return false;
  }

/*--validando modelo---------------------------------------------------------*/
  if(modelo.value=="")
  {
    alert("LLENE EL CAMPO MODELO")
    return false;
  } 
  for(var i=0;i<modelo.value.length; i++){
    if(isNaN(parseInt(modelo.value.charAt(i)))!=true)
    {
      alert("SOLAMENTE SE PERMITEN LETRAS EN CAMPO MODELO");
      return false;
    }
  }
  if(modelo.value.length>20)
  {   
    alert("LAS LETRAS DEL CAMPO MODELO EXCEDE EL LIMITE DE LETRAS, DEBE SER MENOR A 20 LETRAS");
    return false;
  }
  if(modelo.value.length<3)
  {   
    alert("EL CAMPO MODELO DEBE SER POR LO MENOS MAYOR A 3 LETRAS");
    return false;
  }

  /*--validando placa---------------------------------------------------------*/

if(placa.value.length != 5){
  alert("introduzca cantidad de 5 digitos exactos en el campo placa");
  return false;
}

for(var i=0;i<placa.value.length;i++){
  if(isNaN(parseInt(placa.value.charAt(i)))==true){
    alert("solo se permiten numeros enteros");
    return false;
    }
}

/*--validando anio---------------------------------------------------------*/
if(anio.value.length != 4){
  alert("introduzca cantidad de 4 digitos exactos en el campo anio");
  return false;
}

for(var i=0;i<anio.value.length;i++){
  if(isNaN(parseInt(anio.value.charAt(i)))==true){
    alert("solo se permiten numeros enteros");
    return false;
    }
}
/*--validando chasis---------------------------------------------------------*/
if(chasis.value.length != 7){
  alert("introduzca cantidad de 7 digitos exactos en el campo chasis");
  return false;
}

for(var i=0;i<chasis.value.length;i++){
  if(isNaN(parseInt(chasis.value.charAt(i)))==true){
    alert("solo se permiten numeros enteros");
    return false;
    }
}

/*--validando bateria---------------------------------------------------------*/
  if(bateria.value=="")
  {
    alert("LLENE EL CAMPO BATERIA")
    return false;
  } 
  for(var i=0;i<bateria.value.length; i++){
    if(isNaN(parseInt(bateria.value.charAt(i)))!=true)
    {
      alert("SOLAMENTE SE PERMITEN LETRAS EN EL CAMPO BATERIA");
      return false;
    }
  }
  if(bateria.value.length>20)
  {   
    alert("LOS CARACTERES DE CAMPO BATERIA EXCEDEN LIMITE DE LETRAS, DEBE SER MENOR A 20 DIGITOS");
    return false;
  }
  if(bateria.value.length<4)
  {   
    alert("EL CAMPO BATERIA DEBE SER POR LO MENOS MAYOR A 4 DIGITOS");
    return false;
  }

/*--validando PRECIO---------------------------------------------------------*/

if(precio.value.length != 6){
  alert("introduzca cantidad de 6 digitos exactos en el campo precio");
  return false;
}

for(var i=0;i<precio.value.length;i++){
  if(isNaN(parseInt(precio.value.charAt(i)))==true){
    alert("solo se permiten numeros enteros");
    return false;
    }
}

}








 


  