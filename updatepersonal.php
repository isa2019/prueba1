<?php
	include 'sesion.php';//Autor: Lic. Marco Antonio dorado Goméz
	include "lib/config.php";
	include "lib/Database.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale=1.0, minimum-scale=1.0">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="shortcut icon" type="image/x-icon" href="img/ico.jpg">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/estilo.css">
		<title>Registro de Auto</title>
</head>
<body>
	<section class="container">
		<?php
		$id= $_GET['id_p'];
		$db= new Database();
		$query="SELECT * FROM tbl_personal WHERE id_p=$id";
		$getData=$db->select($query)->fetch_assoc();

		if(isset($_POST['submit'])){
		
	
	$nombre=mysqli_real_escape_string($db->link, $_POST['nombre']);
	$apellido=mysqli_real_escape_string($db->link, $_POST['apellido']);
	$cargo=mysqli_real_escape_string($db->link, $_POST['cargo']);
	$salario=mysqli_real_escape_string($db->link, $_POST['salario']);
	$foto=(isset($_FILES['foto']['name']))?$_FILES['foto']['name']:"";
		if($nombre == '' || $apellido == '' || $cargo == '' || $salario == '' || $foto == ''){
			$error="Los campos no deben estar vacios!!!";
			}else{

				$fecha=new DateTime();
				$nomArchivo=($foto!="")?$fecha->getTimestamp()."_".$_FILES["foto"]["name"]:"avatar.png";
				$tmpfoto=$_FILES["foto"]["tmp_name"];
				if($tmpfoto !=""){
					move_uploaded_file($tmpfoto, "img/".$nomArchivo);
					$sentencia="SELECT * FROM tbl_personal WHERE id_p=$id";
					$read=$db->select($query);

				}
				if(isset($nomArchivo["foto"])){
					if(file_exists("img/".$nomArchivo["foto"])){
						if($nomArchivo['foto']!="avatar.png"){
							unlink("img/".$nomArchivo["foto"]);
						}
					}

				}

		//$pass_cifrado = password_hash($contra, PASSWORD_DEFAULT);//encriptando la contraseña
		$query="UPDATE tbl_personal SET nombre= '$nombre', apellido='$apellido', cargo='$cargo', salario='$salario', foto='$nomArchivo' WHERE id_p=$id";
		$update = $db->updatepersonal($query);
	}
}
?>
<?php
if(isset($_POST['delete'])){
	$query ="DELETE FROM tbl_personal WHERE id_p=$id";
	$deleteData=$db->deletepersonal($query);
}
?>

		<div class="row my-5">
			<?php
			 if(isset($error)){
			 	echo"<div class='alert alert-danger'>".$error."</span></div>";
			 }
			 ?>
			<form class="login" action="updatepersonal.php?id_p=<?php echo $id;?>" method="POST" enctype="multipart/form-data">
				<h2><center>Actualizar Datos de Personal</center></h2>
				<div class="form-group">
				<label class="text-info">Nombre:(*)</label>
				<input type="text" maxlength="50" name="nombre" id="nombre" value="<?php echo $getData['nombre'] ?>" placeholder="Introduzca Nombre" class="form-control">
				</div>
				<div class="form-group">
				<label class="text-info">Apellido:(*)</label>
				<input type="text"  name="apellido" id="apellido" value="<?php echo $getData['apellido'] ?>" placeholder="Introduzca Apellido" class="form-control">
				</div>
						 <div class="form-group">
			     <label type="cargo" class="cargo">cargo:</label>
      				<select class="for-control" name="cargo">
      				<option values="secretaria">SECRETARIA</option>
      				<option values="administrador">ADMINISTRADOR</option>
      				<option values="auxiliar">AUXLIAR</option>
      				</select>
      			</div>
				<div class="form-group">
			     <label type="salario" class="salario">SALARIO:</label>
      				<select class="for-control" name="salario">
      				<option values="basico">1500</option>
      				<option values="minimo">2500</option>
      				<option values="regular">3500</option>
      			    </select>
      			</div>
					<div class="form-group">
					<label class="imagen"> Imagen:(*) </label>
					<input type="file" accept="image/*" name="foto" id="foto" value="" placeholder="" class="form-control">
				</div>


				<center>
					<button type="submit" name="submit" value="Update" class="btn btn-primary">Guardar</button>
					<button type="submit" name="delete" value="Delete" class="btn btn-danger">Eliminar</button>
					<a href="listaauto.php" class="btn btn-success">Cancelar</a><br><br>
					

				</center>
			</form>

		</div>
							<div class="col text-center"><!-- para que un boton este centrado -->

	<span><a class="btn btn-info btn-default btn-lg" href="principal.php">VOLVER A PRINCIPAL</a></span>
    </div>
	</section>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery-1.12.3.min.js"></script>
</body>
</html>