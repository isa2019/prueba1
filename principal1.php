<?php
	include 'sesion.php';
	include "lib/config.php";
	include "lib/Database.php";
?>

<!DOCTYPE html>
<html>
<head>
	<title>PRINCIPAL</title>
	<link rel="shortcut icon" type="image/x-icon" href="img/ico.jpg">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link rel="stylesheet" link href="css/bootstrap.min.css" type="text/css">
		<meta name="description" content="Esta es una web inscripcion">
	<meta name="KEYWORDS" content="páginas, web, tecnologia">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/fondo.css">
	<link rel="stylesheet" type="text/css" href="css/galeria.css">
</head>
<body>
	
<h2>"BIENVENIDO A NUESTRA WEB ARTES GRAFICAS DRACO"</h2>

		<div class="container">
		<div class="alert alert-secondary" role="alert">
             <P class="text-justify">Imprenta es la técnica industrial que permite reproducir, en papel o materiales similares, textos y figuras mediante tipos, planchas u otros procedimientos.</P>
        </div>
			
		<div class="row">
		<div class="col-X-12 col-md-6 col-lg-6">
			<a href="registro.php"><img src="img/01.jpg" class="img-fluid imagi"></a>
			<p class="text-primary">CREAR USUARIO</p>
			
			
		</div>
		<div class="col-X-12 col-md-6 col-lg-6">
			<a href="personal.php"><img src="img/02.jpg" class="img-fluid imagi"></a>
			<p class="text-primary">CREAR PERSONAL</p>
			
			
		</div>
		<div class="col-X-12 col-md-6 col-lg-6">
			<a href="inventario.php"><img src="img/03.jpg" class="img-fluid imagi" ></a>
			<p class="text-primary">INVENTARIO DE IMPRENTA</p>
			
			
		</div>
			<div class="col-X-12 col-md-6 col-lg-6">
			<a href="articulo.php"><img src="img/04.jpg" class="img-fluid imagi" ></a>
			<p class="text-primary">NOTICIAS DE IMPRENTA</p>
			
			
		</div>




	
		
		<span><a class="btn btn-info btn-block btn-lg btn-block" href="MostrarArticulo.php">MOSTRAR NOTICIAS</a></span>
			<span><a class="btn btn-warning btn-block btn-lg btn-block" href="index.php">VOLVER AL PRINCIPIO</a></span>
			<span><a class="btn btn-danger btn-block btn-lg btn-block" href="logout.php">SALIR DE LA CUENTA</a></span>
		



		
	</div>
	</div>
	<script  src="js/jquery-3.3.1.min.js"></script>
	<!--<script  src="js/popper.min.js"></script>-->
	<!--<script  src="js/bootstrap.min.js"></script>-->
	<script src="js/galeria.js"></script>
</body>
</html>